YeFiQuav1 :Is a beta version of a software that can  be useful to process and get some relevant morphometric measures from  Yellow Fish Images. 

In order to use YeFiQuav project please go ahead with the followed steps and configurations:

1 - Put a copy of your original images inside of folders and subfolders agree with corresponding to specimens and development stages.
2 - Run the script macro_crop.ijm using imageJ FIJI in order to semi-manual check and crop the relevant image ROIs.
3 - Run the script YeFiQuav1.py in order to automatically process the samples and get morphometric measures from the specimens. As output you will get a grayscale image sowhing the fitted MIC and MMC areas in the sample folder, and a csv file with the results for the whole set of images founded at the input_folder.



In order to succesfully run the script  macro_crop.ijm , please do as follow:
download FIJI ( imageJ 1.52p) from: 
https://imagej.net/Fiji
upgrade and just you are ready to run.

In order to succesfully run the YeFiQuav1.py please  install conda following the  next guide:
Under linux enviroment (please find in internet more information to get it done under windows ~ similar)
1) Download miniconda version of anaconda installer:
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
2) Install miniconda
bash Miniconda3-latest-Linux-x86_64.sh
3) Create an environment (Python >= 3.6.7) using the required package list.
conda create -n ImaProcII --file package-list.txt

please open python and check that all the following packages where corretly installed

#####
import numpy
import cv2
import pandas
import scipy
import PIL
import skimage
####

...If you get   some ERROR like...
ModuleNotFoundError: No module named 'xxx'
please install using pip the same version from  package-list.txt

for ex.:
python -m pip install pandas==0.24.2
python -m pip install pillow==5.4.1
python -m pip install scikit-image==0.15.0


## After these steps will be ready to run  YeFiQuav1.py 

In order to run YeFiQuav1.py please do as follow:
1- open the created environment:
source activate OpenImaII

2- Inside the environment
run the script using python:

python YeFiQuav1.py "input_folder"
(important!!!  "input_folder" has to be at the same folder than the script, 
otherwise please add the correct path to the input folder containing the cropped images.)

You have to get some output like these....

Processing folder: input_folder
File:, input_folder/Ctowsendi2/DL3/Captured 2x-lupa 1.jpg, MIC: 410, MCC: 198, GAPpx: 43045 
File:, input_folder/Ctowsendi2/DL9/Captured 1x-lupa 18.jpg, MIC: 772, MCC: 344, GAPpx: 183294 
....
....
File:, input_folder/Ctowsendi2/DL25/Captured 08-lupa.jpg, MIC: 1240, MCC: 616, GAPpx: 7276 
File:, input_folder/Ctowsendi2/DL9/Captured 1x-lupa 18.jpg, MIC: 772, MCC: 344, GAPpx: 183294 

And you will able to see the printed processed image at each sample folder and a 
