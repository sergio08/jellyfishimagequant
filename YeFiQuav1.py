import os
import sys
import time

import pandas as pd 
import numpy as np
from PIL import Image
from scipy import ndimage as ndi
#import matplotlib.pyplot as plt
 
#skimage


from skimage.draw import circle_perimeter
from skimage import img_as_ubyte , img_as_float , exposure 
from skimage.measure import regionprops
from skimage.transform import resize
from skimage.io import imsave
from skimage import filters ,morphology, color, feature
from skimage.filters import sobel, threshold_otsu
from skimage.morphology import remove_small_objects

#opencv
import cv2

#run
# python YeFiQuav1.py input_test



def scan_path(rootdir):
    """ 
        This method return a dataframe with all scanned files and its respective folder (classes)
         names
    """
    frame = list()
    for root, dirs, files in os.walk(rootdir, followlinks=True): # scan one by one
        files = [os.path.join(root, f) for f in files]
        # Create a dataset with all the
        df = pd.DataFrame({"path": files})
        # Set as tag a concatenation of the subdirectories
        df["file"] = [os.path.basename(item) for item in df.path.values]
        df["id"] = [os.path.basename(os.path.dirname(os.path.dirname(item))) +'_'+ os.path.basename(os.path.dirname(item)) for item in df.path.values ]
        frame.append(df)
    # Concat all dataframes
    df = pd.concat(frame).reset_index(drop=True)
    #df= df[[(".tif" in i.lower() ) or (".jpg" in i )  or (".tiff" in i.lower() )  for i in df.file]]
    df= df[[ (".jpg" in i ) for i in df.file]]
    return df

def getImageArrayUsingPIL(PathToFile, resize=False, resize_method="BILINEAR" ,to_gray=False, w=224, h=224, verbose=False):
    """
    Method to get Images from file using PIL
    more info at: https://pillow.readthedocs.io/en/stable/
    IF resize True  use resise_method =  BICUBIC , ANTIALIAS, BILINEAR,LANCZOS... PIL DEFAULT= NEAREST 
    In latest versions of PIL A new PIL.Image.LANCZOS constant was added instead of ANTIALIA,
    The ANTIALIAS constant is left for backward compatibility and is an alias for LANCZOS
    IF ( (w*h) < (img.shape[0]*img.shape[1]) ) use ANTIALIAS else use default or selected method
    
    IF to_gray true:
    convert tograyscale "L" (8-bit pixels, black and white)  using  ITU-R 601-2  L = R * 299/1000 + G * 587/1000 + B * 114/1000
    
    """
    
    method={"NEAREST":0, "BICUBIC":3, "ANTIALIAS":1, "BILINEAR":2 ,"LANCZOS":1}
    import PIL
    import numpy as np
    img = PIL.Image.open(PathToFile)
    if verbose:
        print('PIL version:',PIL.__version__)
        print("INPUT ARRA  size: " + str(img.size) + " mode: " + img.mode +" Format: " + img.format )
        
    if to_gray:
        img = img.convert("L") # (8-bit pixels, black and white

    if resize:
        if ( (w*h) < (img.size[0]*img.size[1]) ):
            img = img.resize((w,h), resample= 1 ) #use ANTIALIAS
        else:
            img = img.resize((w,h), resample= method[resize_method]) # default or selected

    channel = len(img.getbands())
    img = np.array(img)
    if verbose:
        print( "OUTPUT ARRAY size: " + str(img.shape) + " Channels: " +  str(channel) + " max_pix: "+ str(img.max())+ " min_pix: "+ str(img.min())+ " mean_pix: "+ str(img.mean())+ " dtype: "+ str(img.dtype) )
        
    return img

def average_hash(path, ima_red):
    """ 
        This method return a perceptual hash as a binary string (classes)
         names
    """
    def Boolean_to_BinString(arr, ima_red):
        bit_string = ''.join(str(b) for b in 1 * arr.flatten())
        return(bit_string)
    if ima_red < 2:
        raise ValueError("Hash size must be greater than or equal to 2")
    image = Image.open(path)
    # reduce size and complexity, then covert to grayscale
    image = image.convert("L").resize((ima_red, ima_red), Image.ANTIALIAS)
    pixels = np.asarray(image)
    # find average pixel value; 'pixels' is an array of the pixel values, ranging from 0 (black) to 255 (white)
    avg = pixels.mean()
    #print(avg)
    # create string of bits
    diff = pixels > avg
    # make a hash
    return Boolean_to_BinString(diff, ima_red)

def getHistEqualized(img):
    """ 
        This method return the histogram equalization for a RGB image 
    """
    new_image = np.zeros(img.shape, img.dtype)   
    output_image=[]
    assert len(img.shape) == 3
    for ch in range(len(img.shape)):
        rows, columns = img[:,:,ch].shape
        hist, bins = np.histogram(img[:,:,ch].flatten(),256,[0,256])
        #// compute the cumulative histogram (CDF)
        cdf = hist.cumsum()
        #print(cdf)
        lookup_table = cdf *  (255.0 / (img[:,:,ch].shape[0] * img[:,:,ch].shape[1] ) ) 
        #print(lookup_table)
        #// equalize the image:
        new_image[:,:,ch] = np_LUT(img[:,:,ch].astype('uint8') , lookup_table.astype('uint8')).reshape((rows, columns))
        #print(output_image)
    return( new_image.astype('uint8') )

def np_LUT(img , lookup_table):
    """ 
        This method modify a image using as input a lookup table
    """
    if len(img.shape) == 3:
        new_image = np.zeros(img.shape, img.dtype)
        for i in range(len(img.shape)):
            rows, columns = img[:,:,i].shape
            tmp = np.array(lookup_table).ravel()[img[:,:,i].ravel()]
            new_image[:,:,i] = tp.reshape((rows, columns))
    elif len(img.shape) == 2:
        rows, columns = img.shape
        tmp = np.array(lookup_table).ravel()[img.ravel()]
        new_image = tmp.reshape((rows, columns))
    else:
        print("please use a numpy array image format")
    return(new_image)


def otsu_val(imgArray):
    """ 
        This method return the ostu binarization threshold
    """
    res = threshold_otsu(imgArray)
    #print("outsu threshold: ", res)
    return(res)

def GammaTr(img, gamma=2.5):
    """ 
        This method  generate a gamma transformation
    """
    return (np.clip( (np.power(img/255, gamma) *255) , 0, 255).astype(int))


def getImage(pathToImage, to_gray=False):
    img = getImageArrayUsingPIL(pathToImage, to_gray=to_gray)
    scaleFactor = 1
    if img.shape[0]>1000:
        img = img_as_ubyte(resize(img, (img.shape[0] // 4, img.shape[1] // 4), anti_aliasing=True))
        scaleFactor = 4
    return(img, scaleFactor)


def proc3(img):
    img0 = getHistEqualized(img)
    img0 = GammaTr(img0, gamma=0.25)
    img1 = (img0[:,:,2] < otsu_val(img0[:,:,2])).astype('uint8')
    kernel = np.ones((5,5), np.uint8) 
    img2 = cv2.dilate(img1, kernel, iterations=2) 
    img3 = cv2.erode(img2, kernel, iterations=1)
    kernel = np.ones((3,3),np.uint8)
    img4 = cv2.morphologyEx(img3,cv2.MORPH_CLOSE,kernel, iterations = 4)
    img5 = ndi.binary_fill_holes(img4)
    img6 = remove_small_objects(img5, 10000, connectivity=1)
    img =img6.astype(int)
    return(img)

def proc2(img):
    img0 = 255-img
    img1 = (img0[:,:,2] < otsu_val(img0[:,:,2])).astype('uint8')
    kernel = np.ones((5,5), np.uint8) 
    img2 = cv2.dilate(img1, kernel, iterations=2) 
    img3 = cv2.erode(img2, kernel, iterations=1)
    kernel = np.ones((3,3),np.uint8)
    img4 = cv2.morphologyEx(img3,cv2.MORPH_CLOSE,kernel, iterations = 4)
    img5 = ndi.binary_fill_holes(img4)
    img6 = remove_small_objects(img5, 10000, connectivity=1)
    img =img6.astype(int)
    return(img)

def proc1(img):
    img1 = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    img2 = clahe.apply(img1.astype('uint8'))
    img3 = feature.canny(img2/255.)
    img4 = morphology.binary_dilation( img3.astype("int8"))
    seed= np.copy(img4)
    seed[1:-1, 1:-1] = img4.max()
    img5 =  img_as_ubyte(morphology.reconstruction(seed,img4,method='erosion') ) 
    img6 = ndi.binary_fill_holes(img5)
    img7 = remove_small_objects( img6, 10000, connectivity=1)
    img =img7.astype(int)
    return(img)

def getImaProc(img, phash):    
    c3,c0,c1,c2= 63903 , 64273, 3680, 52932
    target = int(phash,2) #63901
    #target = 63901
    hash_comp = np.array([bin (c0^target).count("1"),bin (c1^target).count("1"), bin (c2^target).count("1"), bin (c3^target).count("1")   ])
    prototype = ["c0","c1","c2","c3"]
    closeProt = prototype [ np.where(hash_comp== hash_comp.min())[0][0]]
    if closeProt =="c0":
        return( proc1(img))
    if closeProt =="c1":
        return( proc2(img))
    if closeProt =="c2":
        return( proc1(img))
    if closeProt =="c3":
        return( proc3(img))


def getCirMask( centerX, centerY, rad, image , masked=False ):
    rr, cc = circle_perimeter(centerX.astype(int), centerY.astype(int), rad +1)
    cir_mask = np.zeros(np.shape(image))
    rr[rr.max() >= image.shape[0]] = image.shape[0]-1
    cc[cc.max() >= image.shape[1]] = image.shape[1]-1
    cir_mask[rr, cc] = 1
    if masked:
        masked = image.copy()
        masked[rr, cc] = 2
        return(cir_mask , masked) 
    else:
        return(cir_mask)
    
def getMCC(centerX, centerY, MIC, image):
    for j in range(1 , MIC - 1):
        mask = getCirMask( centerX, centerY,  MIC- j , image , masked=False )
        if ( len(mask[mask>0]) == np.sum(np.multiply(image, mask.astype(int))>0)):
            tmp_px = np.multiply(image, mask.astype(int)).sum() 
            tmp_rad = (MIC - j)
            MCC = tmp_rad
            return(MCC)
        
def createAnnularMask( centerX, centerY, big_radius, small_radius, image , masked=False):
    Y, X = np.ogrid[:image.shape[0], :image.shape[1]]
    distance_from_center = np.sqrt((X - centerY)**2 + (Y-centerX)**2)
    mask = (small_radius <= distance_from_center) & (distance_from_center <= big_radius)
    return(mask)



def main():
    if(len(sys.argv)<2):
        print("Use: YeFiQuav1.py, input_folder")
        quit()
    in_folder = sys.argv[1]
    
    print("Processing folder:", in_folder)
    df = scan_path(in_folder)  
    print(df.head())
    metrics=[]
    for i, pathToImage in enumerate(df.path.values):
        nam = os.path.dirname(pathToImage) + "/processed_" + str(time.strftime("%Y%m%d_%H%M%S"))+".png"
        try:
            img, scaleFact = getImage(pathToImage)
            phash = average_hash(pathToImage, 4)
            img = getImaProc(img, phash)
            prop = regionprops(img)
            min_row, min_col, max_row, max_col = prop[0].bbox
            center = prop[0].centroid
            MIC = (np.max([max_row - min_row , max_col - min_row])/2).astype(int) 

            MICmk = getCirMask( center[0].astype(int), center[1].astype(int), MIC + 1, img , masked=False)
            MCC  =  getMCC(center[0].astype(int), center[1].astype(int), MIC + 1, img)

            MCCmk = getCirMask( center[0].astype(int), center[1].astype(int), MCC, img , masked=False )
            ringMask = createAnnularMask(center[0].astype(int), center[1].astype(int), MIC, MCC ,img, masked=False )
            GAPpx = ringMask.sum() - np.multiply(img,ringMask).sum()

            metrics.append((i, MIC*2* scaleFact, MCC*2* scaleFact, GAPpx* scaleFact ) )
            img[MICmk==1]=3
            img[MCCmk==1]=3
            img[img==1]=128
            img[img==3]=255
            print('File:, {}, MIC: {}, MCC: {}, GAPpx: {} ' .format(pathToImage, MIC*2* scaleFact, MCC*2* scaleFact,GAPpx*scaleFact  ))
            cv2.imwrite(nam, img) 
        except:
            metrics.append((i,0,0,0 ) )
            img = np.zeros((224,224))
            print('File: {}, MIC: {}, MCC: {}, GAPpx: {} ' .format(pathToImage, 0, 0, 0  ))
            cv2.imwrite(nam, img) 
    df_res = pd.concat([df.reset_index(drop=True), pd.DataFrame(metrics)], axis=1)
    df_res.columns = ['path', 'file', 'id','proc','MICpx','MCCpx','GAPpx']    
    df_res.to_csv(in_folder+ time.strftime("%Y%m%d_%H%M%S")+".csv", index=None)
    
    
if __name__ == "__main__":
    main()
